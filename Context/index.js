import React, { createContext, useContext, useReducer } from "react";

const initialState = {
  messageType: "",
  aircraftID: "",
  flightRules: "",
  typeOfAircraft: "",
  equipment: "",
  departure: "",
  route: "",
  destination: "",
  otherInfo: "",
};

const reducer = (prevState, action) => {
  switch (action.type) {
    case "MESSAGE-TYPE": {
      return { ...prevState, messageType: action.payload };
    }
    case "AIRCRAFT-ID": {
      return { ...prevState, aircraftID: action.payload };
    }
    case "FLIGHT-RULES": {
      return { ...prevState, flightRules: action.payload };
    }
    case "TYPE-OF-AIRCRAFT": {
      return { ...prevState, typeOfAircraft: action.payload };
    }
    case "EQUIPMENT": {
      return { ...prevState, equipment: action.payload };
    }
    case "DEPARTURE": {
      return { ...prevState, departure: action.payload };
    }
    case "ROUTE": {
      return { ...prevState, route: action.payload };
    }
    case "DESTINATION": {
      return { ...prevState, destination: action.payload };
    }
    case "OTHER-INFO": {
      return { ...prevState, otherInfo: action.payload };
    }
    default: {
      return prevState;
    }
  }
};

const stateCtx = createContext(initialState);
const dispatchCtx = createContext(() => 0);

export const Provider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <dispatchCtx.Provider value={dispatch}>
      <stateCtx.Provider value={state}>{children}</stateCtx.Provider>
    </dispatchCtx.Provider>
  );
};

export const useDispatch = () => {
  return useContext(dispatchCtx);
};

export const useGlobalState = () => {
  const state = useContext(stateCtx);
  return state;
};
