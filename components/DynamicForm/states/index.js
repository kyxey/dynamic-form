import { useReducer } from "react";

const initialState = {
  models: [],
};

const reducer = (prevState, action) => {
  switch (action.type) {
    case "MODELS": {
      return { ...prevState, models: action.payload };
    }
    default: {
      return prevState;
    }
  }
};

function useDynamicFormStates() {
  const [{ models }, dispatch] = useReducer(reducer, initialState);

  const changeState = (type, input) => {
    dispatch({ type: type, payload: input });
  };

  return { models, changeState };
}

export default useDynamicFormStates;
