import React, { useEffect, useRef } from "react";
import useDynamicFormStates from "./states";
import styles from "./index.module.scss";

export default function DynamicForm(props) {
  const { models, changeState } = useDynamicFormStates();
  const elRefs = useRef([null]);
  const myResults = useRef(null);

  useEffect(() => {
    changeState("MODELS", props.model);
    elRefs.current = elRefs.current.slice(0, props.model.length);
  }, [props.model]);

  function onSubmit(e) {
    e.preventDefault();
    if (props.onSubmit) {
      props.onSubmit();
    }
  }
  function onChangeInput(e, key) {
    if (props.onChange) {
      props.onChange(e, key);
    }
  }
  function renderForm() {
    const formUI = models.map((eachRow, i) => {
      return (
        <div key={i} className={styles.formGroup}>
          {eachRow.map((each) => {
            return (
              <div key={each.key} className={styles.formRow}>
                <label
                  className={styles.formLabel}
                  key={"l" + each.key}
                  htmlFor={each.key}
                >
                  {each.label}
                </label>
                {each.type === "textarea" ? (
                  <textarea
                    key={"i" + each.key}
                    {...(each.props || {})}
                    ref={(el) => {
                      elRefs.current[i] = { el: el || null, key: each.key };
                    }}
                    className={styles.formInput}
                    onChange={(e) => {
                      onChangeInput(e, each.key);
                    }}
                  ></textarea>
                ) : (
                  <input
                    key={"i" + each.key}
                    {...(each.props || {})}
                    ref={(el) => {
                      elRefs.current[i] = { el: el || null, key: each.key };
                    }}
                    className={styles.formInput}
                    type={each.type || "text"}
                    onChange={(e) => {
                      onChangeInput(e, each.key);
                    }}
                  />
                )}
              </div>
            );
          })}
        </div>
      );
    });
    return formUI;
  }

  const title = props.title || "Dynamic Form";
  return (
    <div className={props.className}>
      <h2>{title}</h2>
      <form
        className={styles.dynamicForm}
        onSubmit={(e) => {
          onSubmit(e);
        }}
      >
        {renderForm()}
        <div className={styles.formButton}>
          <button className={styles.button} type="submit">
            Submit
          </button>
          <button className={styles.button} type="reset">
            Clear values
          </button>
        </div>
        <div ref={myResults}></div>
      </form>
    </div>
  );
}
