import {
  Entity,
  Column,
  ObjectIdColumn,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
} from "typeorm";
import { v4 } from "uuid";

@Entity({ name: "Flight" })
export class Flight {
  @ObjectIdColumn()
  _id;

  @PrimaryGeneratedColumn("uuid")
  id;

  @Column({ type: "string" })
  message;

  @Column({ type: "string" })
  aircraftType;

  @Column({ type: "string" })
  departure;

  @Column({ type: "string" })
  route;

  @Column({ type: "string" })
  destination;

  @Column({ type: "string" })
  otherInfo;

  @CreateDateColumn()
  createdAt;

  @UpdateDateColumn()
  updateAt;

  @BeforeInsert()
  addId() {
    this.id = v4();
  }
}
