import "reflect-metadata";
import { createConnection } from "typeorm";
import { Flight } from "../flight/index.entity";

export async function connect() {
  return new Promise(async (resolve, _reject) => {
    if (process.env.DB_URL) {
      const connection = await createConnection({
        name: "default",
        type: "mongodb",
        url: process.env.DB_URL,
        entities: [Flight],
        synchronize: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      resolve(connection);
    } else {
      const connection = await createConnection({
        name: "default",
        type: "mongodb",
        host: process.env.DB_HOST || "localhost",
        port: parseInt(process.env.DB_PORT || "27017"),
        username: process.env.DB_USERNAME || "admin",
        password: process.env.DB_PASSWORD || "admin",
        database: process.env.DB_NAME || "flight",
        entities: [Flight],
        synchronize: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      resolve(connection);
    }
  });
}
