import { useReducer } from "react";

const initialState = {
  output: "",
};

const reducer = (prevState, action) => {
  switch (action.type) {
    case "OUTPUT": {
      return { ...prevState, output: action.payload };
    }
    default: {
      return prevState;
    }
  }
};

function useIndexStates() {
  const [{ output }, dispatch] = useReducer(reducer, initialState);

  const changeState = (type, input) => {
    dispatch({ type: type, payload: input });
  };

  return { output, changeState };
}

export default useIndexStates;
