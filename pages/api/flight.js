import { ApolloServer, gql } from "apollo-server-micro";
import { connect } from "../../api/connection";
import { Flight } from "../../api/flight/index.entity";

const typeDefs = gql`
  input FlightDataInput {
    message: String!
    aircraftType: String!
    departure: String!
    route: String!
    destination: String!
    otherInfo: String!
  }
  type FlightData {
    message: String!
    aircraftType: String!
    departure: String!
    route: String!
    destination: String!
    otherInfo: String!
  }
  type Query {
    flights: [FlightData]!
  }
  type Mutation {
    addFlight(data: FlightDataInput): FlightData!
  }
`;
const resolvers = {
  Query: {
    flights: async (_parent, _args, _context) => {
      const connection = await connect();
      const results = await connection.getRepository(Flight).find({});
      connection.close();
      return results;
    },
  },
  Mutation: {
    addFlight: async (_parent, args, _context) => {
      const connection = await connect();
      const flightRepository = connection.getRepository(Flight);
      const newFlight = new Flight();
      newFlight.message = args.data.message;
      newFlight.aircraftType = args.data.aircraftType;
      newFlight.departure = args.data.departure;
      newFlight.route = args.data.route;
      newFlight.destination = args.data.destination;
      newFlight.otherInfo = args.data.otherInfo;
      const result = await flightRepository.save(newFlight);
      connection.close();
      return result;
    },
  },
};

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  context: () => {
    return {};
  },
});

const handler = apolloServer.createHandler({ path: "/api/flight" });

export const config = {
  api: {
    bodyParser: false,
    cors: true,
  },
};

export default handler;
