import Head from "next/head";
import React from "react";
import ApolloClient, { gql } from "apollo-boost";
import { useGlobalState, useDispatch } from "../Context";
import useIndexStates from "../states";
import DynamicForm from "../components/DynamicForm";
import styles from "./index.module.scss";

const client = new ApolloClient({ uri: "http://localhost:3000/api/flight" });

export default function Home() {
  const {
    messageType,
    aircraftID,
    flightRules,
    typeOfAircraft,
    equipment,
    departure,
    route,
    destination,
    otherInfo,
  } = useGlobalState();
  const { output, changeState } = useIndexStates();
  const dispatch = useDispatch();

  function onSubmit() {
    const output = {
      message: `${messageType}-${aircraftID}-${flightRules}`,
      aircraftType: `-${typeOfAircraft}-${equipment}`,
      departure: `-${departure}`,
      route: `-${route}`,
      destination: `-${destination}`,
      otherInfo: `-${otherInfo}`,
    };
    client
      .mutate({
        mutation: gql`
          mutation ADD_FLIGHT(
            $message: String!
            $aircraftType: String!
            $departure: String!
            $route: String!
            $destination: String!
            $otherInfo: String!
          ) {
            addFlight(
              data: {
                message: $message
                aircraftType: $aircraftType
                departure: $departure
                route: $route
                destination: $destination
                otherInfo: $otherInfo
              }
            ) {
              message
              aircraftType
              departure
              route
              destination
              otherInfo
            }
          }
        `,
        variables: {
          message: output.message,
          aircraftType: output.aircraftType,
          departure: output.departure,
          route: output.route,
          destination: output.destination,
          otherInfo: output.otherInfo,
        },
      })
      .then((result) => {
        const stringResult = JSON.stringify(result.data.addFlight, null, "\t");
        changeState(
          "OUTPUT",
          `${result.data.addFlight.message}\n` +
            `${result.data.addFlight.aircraftType}\n` +
            `${result.data.addFlight.departure}\n` +
            `${result.data.addFlight.route}\n` +
            `${result.data.addFlight.destination}\n` +
            `${result.data.addFlight.otherInfo}\n` +
            `${stringResult}`
        );
        console.log(stringResult);
      })
      .catch((error) => {
        console.error(error);
      });
    const jsonOutput = JSON.stringify(output, null, "\t");
    alert(jsonOutput);
  }
  function onChange(e, key) {
    switch (key) {
      case "messageType": {
        return dispatch({ type: "MESSAGE-TYPE", payload: e.target.value });
      }
      case "aircraftID": {
        return dispatch({ type: "AIRCRAFT-ID", payload: e.target.value });
      }
      case "flightRules": {
        return dispatch({ type: "FLIGHT-RULES", payload: e.target.value });
      }
      case "typeOfAircraft": {
        return dispatch({ type: "TYPE-OF-AIRCRAFT", payload: e.target.value });
      }
      case "equipment": {
        return dispatch({ type: "EQUIPMENT", payload: e.target.value });
      }
      case "departure": {
        return dispatch({ type: "DEPARTURE", payload: e.target.value });
      }
      case "route": {
        return dispatch({ type: "ROUTE", payload: e.target.value });
      }
      case "destination": {
        return dispatch({ type: "DESTINATION", payload: e.target.value });
      }
      case "otherInfo": {
        return dispatch({ type: "OTHER-INFO", payload: e.target.value });
      }
      default: {
        break;
      }
    }
  }
  return (
    <div className="container">
      <Head>
        <title>Dynamic Form</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Dynamic Form</h1>
        <DynamicForm
          className="form"
          title="Filed Flight Plan"
          model={[
            [
              {
                key: "messageType",
                label: "Message Type, Number, and Reference Data",
                props: { required: true },
              },
              {
                key: "aircraftID",
                label: "Aircraft ID and SSR Mode and Code",
                props: { required: true },
              },
              {
                key: "flightRules",
                label: "Flight Rules and Type of Flight",
                props: { required: true },
              },
            ],
            [
              {
                key: "typeOfAircraft",
                label: "Type of Aircraft and Wake Turbulence Category",
                props: { required: true },
              },
              {
                key: "equipment",
                label: "Equipment and capabilities",
                props: { required: true },
              },
            ],
            [
              {
                key: "departure",
                label: "Departure aerodrome and time",
                props: { required: true },
              },
            ],
            [
              {
                key: "route",
                label: "Route",
                type: "textarea",
                props: { required: true },
              },
            ],
            [
              {
                key: "destination",
                label:
                  "Destination aerodrome and total estimated elapsed time, destination alternate aerodrome(s)",
                props: { required: true },
              },
            ],
            [
              {
                key: "otherInfo",
                label: "Other information",
                type: "textarea",
                props: { required: true },
              },
            ],
          ]}
          onSubmit={() => {
            onSubmit();
          }}
          onChange={(e, key) => {
            onChange(e, key);
          }}
        />
        <div>
          <h3>Filed Flight Plan Output:</h3>
          <pre className={styles.JsonText}>{output}</pre>
        </div>
      </main>
    </div>
  );
}
